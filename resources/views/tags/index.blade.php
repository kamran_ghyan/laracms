
@extends('main')

@section('title', 'All Posts')

@section('content')


    <div class="row">
        <div class="medium-9 columns">
            <table>
                <thead>
                <th>#</th>
                <th>Name</th>
                <th>Create at</th>
                <th>Update at</th>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{ $category->id  }}</td>
                        <td>{{ $category->name  }}</td>
                        <td>{{ $category->created_at  }}</td>
                        <td>{{ $category->updated_at  }}</td>
                        {{--<td class="text-center"><a href="{{ route('posts.show', $post->id) }}" class="button">View</a> <a href="{{ route('posts.edit', $post->id) }}" class="button">Edit</a> </td>--}}
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="text-center">
               {{-- {{ $posts->links()  }}--}}
            </div>
        </div>
        <div class="medium-3 columns" data-sticky-container>

            {{  Form::open(array('route' => 'categories.store', 'method' => 'POST')) }}

            {{ Form::label('name', 'Name', array('class' => 'text-left')) }}
            {{ Form::text('name', null, array('class' => 'medium-12 columns', 'required' => '', 'maxlength' => '255')) }}


            {{ Form::submit('Create New Category', array('class' => 'button')) }}

            {{ Form::close() }}

        </div>
    </div>
@endsection