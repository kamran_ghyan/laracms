
@extends('main')

@section('title', '| View Post')

@section('content')
    <div class="row column">
        <div class="medium-8 column">
            <h1>{{ $post->title }}</h1>
            <p>{{ $post->body  }}</p>

        </div>
        <div class="medium-3 column">
            <div class="row">

            <dl>
                <dt>URL:</dt>
                <dd><a href="{{url('blog/' . $post->slug)}}"> {{ $post->slug }} </a></dd>
                <dt>Created at:</dt>
                <dd>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</dd>
                <dt>Updated at:</dt>
                <dd>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</dd>
            </dl>
                <div class="row">
                <div class="medium-6 column text-right">
                {{ Html::linkRoute('posts.edit', 'Edit', array($post->id), array('class' => 'button')) }}
                    </div>
                    <div class="medium-6 column text-left">
                {{  Form::open(['route' => ['posts.destroy', $post->id], 'method' => 'delete']) }}
                {{ Form::submit('Delete', array('class' => 'button')) }}
                {{ Form::close() }}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    {{HTML::linkRoute('posts.index', '<< See All Posts', [], ['class' => 'button'])}}
                </div>
            </div>
        </div>
    </div>

@endsection
