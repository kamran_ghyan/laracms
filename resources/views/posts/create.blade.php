
@extends('main')

@section('title', 'Edit Blog Post')
@section('stylesheet')
    {{ Html::style('css/parsley.css') }}
@endsection
@section('content')

    <div class="row column text-center">
        <h1>Edit blog</h1>
        {{  Form::open(array('route' => 'posts.store', 'data-parsley-validate' => '')) }}

            {{ Form::label('title', 'Title', array('class' => 'text-left')) }}
            {{ Form::text('title', null, array('class' => 'medium-12 columns', 'required' => '', 'maxlength' => '255')) }}

            {{ Form::label('slug', 'Slug', array('class' => 'text-left')) }}
            {{ Form::text('slug', null, array('class' => 'medium-12 columns', 'required' => '', 'minlength' => '5', 'maxlength' => '255')) }}

            {{ Form::label('body', 'Body', array('class' => 'text-left')) }}
            {{ Form::textarea('body', null, array('class' => 'medium-12 columns', 'required' => '')) }}

            {{ Form::submit('Create Post', array('class' => 'button')) }}

        {{ Form::close() }}
    </div>
@endsection
@section('scripts')
    {{ Html::script('js/parsley.min.js') }}
@endsection