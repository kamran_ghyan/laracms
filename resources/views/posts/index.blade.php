
@extends('main')

@section('title', 'All Posts')

@section('content')

    <div class="row column text-center">
        <div class="medium-10 column">
            <h1 class="text-left">All Posts</h1>
        </div>
        <div class="medium-2 column">
            <a href="{{ route('posts.create') }}" class="button">Create a New Blog</a>
        </div>
        <div class="medium-12">
            <hr>
        </div>
    </div>
    <div class="row">
        <div class="medium-12">
            <table>
                <thead>
                    <th>#</th>
                    <th>Title</th>
                    <th>Body</th>
                    <th>Create At</th>
                    <th></th>
                </thead>
                <tbody>
                    @foreach($posts as $post)
                    <tr>
                        <td>{{ $post->id  }}</td>
                        <td>{{ $post->title  }}</td>
                        <td>{{ substr($post->body, 0, 25)  }} {{ strlen($post->body) > 25 ? "..." : "" }}</td>
                        <td>{{ date('M j, Y h:ia', strtotime($post->created_at))  }}</td>
                        <td class="text-center"><a href="{{ route('posts.show', $post->id) }}" class="button">View</a> <a href="{{ route('posts.edit', $post->id) }}" class="button">Edit</a> </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="text-center">
                {{ $posts->links()  }}
            </div>
        </div>
    </div>
@endsection
