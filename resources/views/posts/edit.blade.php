@extends('main')

@section('title', 'Create Post')
@section('content')

    <div class="row column">
        {{  Form::model($post, ['route' => ['posts.update', $post->id], 'method' => 'PUT']) }}
        <div class="medium-8 column">
            {{ Form::label('title', 'Title', array('class' => 'text-left')) }}
            {{ Form::text('title', null, array('class' => 'medium-12 columns')) }}
            {{ Form::label('slug', 'Slug', array('class' => 'text-left')) }}
            {{ Form::text('slug', null, array('class' => 'medium-12 columns')) }}
            {{ Form::label('body', 'Body', array('class' => 'text-left')) }}
            {{ Form::textarea('body', null, array('class' => 'medium-12 columns')) }}
        </div>
        <div class="medium-3 column">
            <dl>
                <dt>Created at:</dt>
                <dd>{{ date('M j, Y h:ia', strtotime($post->created_at)) }}</dd>
                <dt>Updated at:</dt>
                <dd>{{ date('M j, Y h:ia', strtotime($post->updated_at)) }}</dd>
            </dl>
            <div class="row">
                {{ Html::linkRoute('posts.show', 'Cancel', array($post->id), array('class' => 'button')) }}
                {{ Form::submit('Update',array('class' => 'button')) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>

@endsection
