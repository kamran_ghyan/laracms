@if (Session::has('success'))
    <p class="primary callout">
        <strong>Success: </strong>{{Session::get('success')}}
    </p>
@endif

@if (count($errors) > 0)
    <p class="primary callout">
        <strong>Erros: </strong>
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error  }}</li>
            @endforeach
        </ul>
    </p>
@endif