<div class="top-bar">
    <div class="top-bar-left">
        <ul class="menu">
            <li class="menu-text">Lara CMS</li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu">
            <li class="{{ Request::is('/') ? "active" : "" }}"><a href="{{url('/')}}">Home</a></li>
            <li class="{{ Request::is('blog') ? "active" : "" }}"><a href="{{url('blog')}}">Blog</a></li>
            <li class="{{ Request::is('about') ? "active" : "" }}"><a href="{{url('about')}}">About</a></li>
            <li class="{{ Request::is('faq') ? "active" : "" }}"><a href="{{url('faq')}}">FAQ</a></li>
            <li class="{{ Request::is('contact') ? "active" : "" }}"><a href="{{url('contact')}}">Contact</a></li>
        </ul>
    </div>
</div>