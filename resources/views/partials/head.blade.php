<meta charset="utf-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Lara CMS | @yield('title') </title>
<link rel="stylesheet" href="{{ asset('css/foundation.min.css') }}">
<link href='http://cdnjs.cloudflare.com/ajax/libs/foundicons/3.0.0/foundation-icons.css' rel='stylesheet' type='text/css'>

@yield('stylesheet')


