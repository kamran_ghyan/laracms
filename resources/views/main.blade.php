<!doctype html>
<html class="no-js" lang="en">
    <head>
        @include('partials.head')
    </head>

    <body>

        @include('partials.nav')

        @include('partials.slider')

            <div class="row" id="content">
                @include('partials.messages')
                @yield('content')
            </div>

        @include('partials.footer')
        @yield('scripts')
    </body>
</html>