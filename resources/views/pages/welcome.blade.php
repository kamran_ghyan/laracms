@extends('main')

@section('title', 'Home Page')

@section('content')

<div class="medium-8 columns">
    @foreach($posts as $post)
        <div class="blog-post">
            <h4>{{$post->title}} <small>{{ date('m/j/Y', strtotime($post->created_at) )}}</small></h4>
            <img class="thumbnail" src="http://placehold.it/850x350">
            <p>{{ substr($post->body, 0, 100)  }} {{ strlen($post->body) > 100 ? "..." : "" }}</p>
            <div class="callout">
                <ul class="menu simple">
                    <li><a href="#">Author: Mike Mikers</a></li>
                    <li><a href="#">Comments: 3</a></li>
                    <li><a href="{{url('blog/' . $post->slug)}}">Read more</a></li>
                </ul>
            </div>
        </div>
    @endforeach
</div>
<div class="medium-3 columns" data-sticky-container>
    <div class="sticky" data-sticky data-anchor="content">
        <h4>Categories</h4>
        <ul>
            <li><a href="#">Skyler</a></li>
            <li><a href="#">Jesse</a></li>
            <li><a href="#">Mike</a></li>
            <li><a href="#">Holly</a></li>
        </ul>
        <h4>Authors</h4>
        <ul>
            <li><a href="#">Skyler</a></li>
            <li><a href="#">Jesse</a></li>
            <li><a href="#">Mike</a></li>
            <li><a href="#">Holly</a></li>
        </ul>
    </div>
</div>
@endsection