
@extends('main')

@section('title', "| $post->title")

@section('content')
    <div class="row column">
        <div class="medium-12 column">
            <h1>{{ $post->title }}</h1>
            <p>{{ $post->body  }}</p>
        </div>
    </div>

@endsection
