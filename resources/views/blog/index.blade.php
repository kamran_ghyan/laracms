@extends('main')

@section('title', 'Blog')

@section('content')
    <div class="medium-12 columns">
        @foreach($posts as $post)
            <div class="blog-post">
                <h4>{{$post->title}} <small>{{ date('m/j/Y', strtotime($post->created_at) )}}</small></h4>
                <img class="thumbnail" src="http://placehold.it/1250x350">
                <p>{{ substr($post->body, 0, 250)  }} {{ strlen($post->body) > 250 ? "..." : "" }}</p>
                <div class="callout">
                    <ul class="menu simple">
                        <li><a href="#">Author: Mike Mikers</a></li>
                        <li><a href="#">Comments: 3</a></li>
                        <li><a href="{{url('blog/' . $post->slug)}}">Read more</a></li>
                    </ul>
                </div>
            </div>
        @endforeach
    </div>
    <div class="text-center">
        {{ $posts->links()  }}
    </div>
@endsection