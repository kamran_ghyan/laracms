<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class BlogController extends Controller
{
    public function getIndex(){
        // Fetch Blos list from DB
        $post = Post::paginate(10);
        // Return to view
        return view('blog.index')->withPosts($post);
    }

    public function getSingle($slug){
        // Fetach from DB
        $post = Post::where('slug', '=', $slug)->first();

        // Return to view
        return view('blog.single')->withPost($post);
    }
}
