<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;

class PagesController extends Controller
{
    /*
     * Process Normally by Controller
     * process variables or params
     * talk to model
     * receive from the model
     * compile or process data from the model if needed
     * pass that data to the correct view
     */

    /**
     * Get a request of the page.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(){
        $posts = Post::orderBy('created_at', 'desc')->limit(5)->get();
        return view('pages.welcome')->withPosts($posts);
    }

    /**
     * Get a request of the page.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAbout(){
        $fname = "Kamran";
        $lname = "Ghyan";

        $fullName = $fname . "" . $lname;
        $email = 'kamranghyan@gmail.com';

        $data['fullname'] = $fullName;
        $data['email'] = $email;

        return view('pages.about')->withData($data);
    }

    /**
     * Get a request of the page.
     *
     * @return \Illuminate\Http\Response
     */
    public function getContact(){
        return view('pages.contact');
    }


}
